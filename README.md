# Promise Faucet

## Motivation

This tools is developed for the specific situation where a high number of promises (HTTP fetch) need to be executed and the server may cause a timeout or simply stall with a regular Promise.all.

The main idea is to make a stack of the promises and give some controls on how fast the stack is consumed.

