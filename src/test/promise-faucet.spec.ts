import {PromiseFaucet} from "../main/promise-faucet";

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);

const expect = chai.expect;


const inputs = Object.freeze([1, 2, 3, 4]);
const expectedOutputs = Object.freeze(["1", "2", "3", "4"]);


describe('Promise Faucet function', function () {

    this.timeout(10000);

    it('should receive an array of values and a promise mapper', () => {
        new PromiseFaucet(inputs, promiseMapper);
    });

    it('should return array of results (R)', () => {
        const sut = new PromiseFaucet(inputs, promiseMapper);

        return sut.start()
            .then(results => {
                expect(results).to.be.a("array").that.is.not.empty;
                expect(results[0]).to.be.a("string");
            })

    });

    it('should return expected values', () => {
        const sut = new PromiseFaucet(inputs, promiseMapper);

        return sut.start()
            .then(results => {
                expect(results).to.has.members(expectedOutputs);
            })

    });

    it('should be pool-size configurable', () => {
        const sut = new PromiseFaucet(inputs, promiseMapper);

        return expect(sut.start(2)).to.eventually.be.an("array").and.has.members(expectedOutputs);

    });

    it('should not be called twice', () => {
        const sut = new PromiseFaucet([], promiseMapper);

        sut.start();

        return expect(sut.start()).to.eventually.be.rejectedWith("Already called");
    })
});

function promiseMapper(value: number): Promise<string> {

    return new Promise((resolve) => {
        console.debug(`Creating promise for ${value}`);
        setTimeout(() => {
            console.debug(`Resolving promise for ${value}`);
            resolve(value.toString());
        }, 1500);
    });
}

