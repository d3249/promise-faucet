import {stringSplitter} from '../main/string-splitter';
import {expect} from 'chai';

const array = Object.freeze([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
describe('Array splitter', () => {

    it('should split in a vector evenly', () => {
        const maxSize = 2;
        const expectedList = [[1,2],[3,4],[5,6],[7,8],[9,10]];

        const result = stringSplitter(array, maxSize);

        expect(result).to.be.an("array").of.length(5);
        expectedList.forEach(expectedValue => expect(result).to.deep.contain(expectedValue));
        expect(result[0]).to.be.an("array").of.length(maxSize);

    });

    it('should split in a vector odd', () => {
        const maxSize = 3;
        const expectedList = [[1,2,3],[4,5,6],[7,8,9],[10]];
        const expectedLengths = [3,3,3,1];

        const result = stringSplitter(array, maxSize);

        expect(result).to.be.an("array").of.length(4);
        expectedList.forEach(expectedValue => expect(result).to.deep.contain(expectedValue));

        const realLengths = result.map( sl => sl.length);

        expect(realLengths).to.be.deep.equal(expectedLengths);

    });

    it('should split in a vector with residue other than 1', () => {
        const maxSize = 4;
        const expectedList = [[1,2,3,4],[5,6,7,8],[9,10]];
        const expectedLengths = [4,4,2];

        const result = stringSplitter(array, maxSize);

        expect(result).to.be.an("array").of.length(3);
        expectedList.forEach(expectedValue => expect(result).to.deep.contain(expectedValue));

        const realLengths = result.map( sl => sl.length);

        expect(realLengths).to.be.deep.equal(expectedLengths);

    });

    it('should work with length 1', () => {
        const maxSize = 1;
        const expectedList = [[1],[2],[3],[4],[5],[6],[7],[8],[9],[10]];
        const expectedLengths = [1,1,1,1,1,1,1,1,1,1];

        const result = stringSplitter(array, maxSize);

        expect(result).to.be.an("array").of.length(10);
        expectedList.forEach(expectedValue => expect(result).to.deep.contain(expectedValue));

        const realLengths = result.map( sl => sl.length);

        expect(realLengths).to.be.deep.equal(expectedLengths);

    });
});