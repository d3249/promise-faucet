import {stringSplitter} from "./string-splitter";

const os = require('os');

export class PromiseFaucet<T, R> {
    private readonly responses: R[];
    private used: boolean;

    constructor(readonly values: readonly T[], private readonly promiseMapper: (v: T) => Promise<R>) {
        this.responses = [];
        this.used = false;
    }

    start(poolSize?: number): Promise<R[]> {

        const finalPoolSize = poolSize || os.cpus().length;

        const subLists: T[][] = stringSplitter(this.values, finalPoolSize);

        return new Promise(async (resolve, reject) => {

            if (this.used) {
                reject("Already called");
            }
            this.used = true;

            for (let i = 0; i < subLists.length; i++) {
                (await Promise.all(subLists[i].map(this.promiseMapper)))
                    .flatMap(partialResult => this.responses.push(partialResult));
            }

            resolve(this.responses);

        });


    }
}


