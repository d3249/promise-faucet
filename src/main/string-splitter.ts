export function stringSplitter<T>(original: readonly T[], maxSize: number): T[][] {

    const finalArray: T[][] = [];
    let innerArray: T[] = [];

    original.forEach((value, idx) => {

        innerArray.push(value);

        if (idx % maxSize == (maxSize - 1) || idx == original.length - 1) {
            finalArray.push(innerArray);
            innerArray = [];
        }


    });

    return finalArray;
}